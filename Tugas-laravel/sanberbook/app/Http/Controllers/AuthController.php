<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.form');
    }

    public function welcome(Request $request)
    {
        $first_name = $request->input('fname');
        $last_name = $request->input('lname');

        return view('page.welcome', ['first_name' => $first_name, 'last_name' => $last_name]);
    }
}
