@extends('layouts.master')

@section('title')
Halaman Daftar
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>
  <form action="welcome" method="POST">
    @csrf
    <label>First name:</label> <br />
    <input type="text" name="fname" /> <br />
    <br />
    <label>Last name:</label> <br />
    <input type="text" name="lname" /> <br />
    <br />
    <label>Gender:</label> <br />
    <input type="radio" name="gender" value="1" /> Male <br />
    <input type="radio" name="gender" value="2" /> Female <br />
    <input type="radio" name="gender" value="3" /> Other <br />
    <br />
    <label>Nationality:</label> <br />
    <select name="nationality">
      <option value="1">Indonesian</option>
      <option value="2">Singaporean</option>
      <option value="2">Malaysian</option>
      <option value="2">Australian</option>
    </select>
    <br />
    <br />
    <label>Language Spoken:</label> <br />
    <input type="checkbox" name="language" value="1" /> Bahasa Indonesia <br />
    <input type="checkbox" name="language" value="2" /> English <br />
    <input type="checkbox" name="language" value="3" /> Other <br />
    <br />
    <label>Bio:</label> <br />
    <textarea name="bio" cols="30" rows="10"></textarea> <br />
    <br />
    <button type="submit">Sign Up</button>
  </form>
@endsection