@extends('layouts.master')

@section('title')
Detail Cast
@endsection

@section('content')
<h1>{{$cast->name}}</h1>
<h5>{{$cast->umur}} Tahun</h5>
<p>{{$cast->bio}}</p>
@endsection