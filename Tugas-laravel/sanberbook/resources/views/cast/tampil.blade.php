@extends('layouts.master')

@section('title')
Halaman Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah cast</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
    <tr>
      <th scope="row">{{$key + 1}}</th>
      <td>{{$item->name}}</td>
      <td>{{$item->umur}}</td>
      <td>
        <form action="/cast/{{$item->id}}" method="POST">
          @csrf
          @method("DELETE")
          <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
          <a href="/cast/{{$item->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
          <input type="submit" value="delete" class="btn btn-danger btn-sm">
        </form>
      </td>
    </tr>
    @empty
    <tr>
      <td>Tidak ada data.</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection