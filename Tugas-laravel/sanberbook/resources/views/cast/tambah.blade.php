@extends('layouts.master')

@section('title')
Tambah Cast
@endsection

@section('content')
<form method="POST" action="/cast">
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" name="name">
  </div>
  <div class="form-group">
    <label>Umur</label>
    <input type="number" class="form-control" name="umur">
  </div>
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection