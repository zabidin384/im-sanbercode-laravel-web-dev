<?php
require 'animal.php';
require 'frog.php';
require 'ape.php';

$sheep = new Animal("shaun");
$sheep->get_name();
$sheep->get_legs();
$sheep->get_cold_blooded();

$kodok = new Frog("buduk");
$kodok->get_name();
$kodok->get_legs();
$kodok->get_cold_blooded();
$kodok->jump();

$sungokong = new Ape("kera sakti");
$sungokong->get_name();
$sungokong->get_legs();
$sungokong->get_cold_blooded();
$sungokong->yell();
?>